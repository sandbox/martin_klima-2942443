<?php

namespace Drupal\field_readonly\Form;

use Drupal\Core\Entity\ContentEntityType;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class FieldReadOnlyConfigForm.
 *
 * @package Drupal\field_readonly\Controller
 */
class FieldReadOnlyConfigForm extends ConfigFormBase {

  /**
   * Gets the configuration names that will be editable.
   *
   * @return array
   *   An array of configuration object names that are editable if called in
   *   conjunction with the trait's config() method.
   */
  protected function getEditableConfigNames() {
    return ['field_readonly.config_form'];
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'field_readonly_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('field_readonly.config_form');

    /** @var \Drupal\Core\Entity\EntityTypeManager $ets */
    $ets = \Drupal::service('entity_type.manager');
    $entityTypes = $ets->getDefinitions();
    $contentEntityTypes = [];
    /** @var \Drupal\Core\Entity\EntityTypeInterface $definition */
    foreach ($entityTypes as $type => $definition) {
      if ($definition instanceof ContentEntityType) {
        $contentEntityTypes[$type] = $definition;
      }
    }
    // @todo Get bundles.
    /** @var \Drupal\Core\Entity\ContentEntityType $contentEntityType */
    foreach ($contentEntityTypes as $entityTypeId => $contentEntityType) {
      $x = $contentEntityType->getBundleLabel();
      $x = $contentEntityType->getBundleEntityType();

    }

    $form['config'] = [
      '#type' => 'textarea',
      '#title' => $this->t('CKEditor Custom Configuration'),
      '#description' => $this->t("Each row is configuration set in format key=value. Don't use quotes for string values."),
      '#default_value' => implode(', ', array_keys($contentEntityTypes)),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('field_readonly.config_form')
      ->set('config', $form_state->getValue('config'))
      ->save();
  }

}
