<?php

namespace Drupal\field_readonly;

use Drupal\Core\Entity\ContentEntityFormInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Form\FormBase;
use Drupal\field\FieldConfigInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ReadOnly.
 *
 * @package Drupal\field_readonly
 */
class ReadOnly {

  /**
   * ReadOnly constructor.
   */
  public function __construct() {
    $this->entityManager = \Drupal::service('entity.manager');
    $this->fieldTypeManager = \Drupal::service('entity_type.manager');
  }

  /**
   * Set read only fields.
   *
   * Modify display form for content entity fields ans set them read only
   * if user has not permission for edit operations.
   *
   * @param array $form
   *   Drupal form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Drupal form state.
   */
  public function readOnlyExecute(array &$form,
                                  FormStateInterface $form_state) {
    /** @var \Drupal\Core\Form\FormBase $formObject */
    $formObject = $form_state->getFormObject();
    if ($this->isContentEntityForm($formObject)) {
      /** @var \Drupal\Core\Entity\EntityFormInterface $formObject */
      $formOperation = $formObject->getOperation();
      // Test if we are adding a new content or editing some existing.
      if (in_array($formOperation, ['edit', 'default'])) {
        /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
        $entity = $formObject->getEntity();
        // @todo Place some configured condition here and use RO mode only for selected entity/bundle.
        if ($entity->bundle() == 'page2') {
          return;
        }
        $entityFields = $this->getEntityFields($entity);

        // Explore each form field element.
        foreach ($entityFields as $entityFieldKey => $entityField) {
          $fieldItemList = $entity->get($entityFieldKey);
          // Test view and edit permissions for the field.
          if (!$fieldItemList->access('edit') && $fieldItemList->access('view')) {
            // Show and disable this field, set CSS class.
            $form[$entityFieldKey]['#disabled'] = TRUE;
            $form[$entityFieldKey]['#access'] = TRUE;
            $form[$entityFieldKey]['#attributes']['class'][] = 'read-only-field';
          }
        }
      }
    }
  }

  /**
   * Load fields by entity.
   *
   * @param string $entityTypeId
   *   Entity type.
   * @param string $entityBundle
   *   Entity bundle.
   *
   * @return array
   *   Array of fields keyed by field machine name.
   */
  public function loadFieldsByEntity($entityTypeId, $entityBundle) {
    $entities = array_filter($this->entityManager->getFieldDefinitions($entityTypeId, $entityBundle), function ($field_definition) {
      return $field_definition instanceof FieldConfigInterface;
    });
    return $entities;
  }

  /**
   * Test if we are editing a ContentEntity.
   *
   * @param mixed $formObject
   *   Form object is result of $form_state->getFormObject().
   *
   * @return bool
   *   TRUE = yes.
   */
  private function isContentEntityForm($formObject) {
    return $formObject instanceof ContentEntityFormInterface;
  }

  /**
   * Get entity fields.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Entity.
   *
   * @return array
   *   Array of fields.
   */
  private function getEntityFields(ContentEntityInterface $entity) {
    return $this->loadFieldsByEntity($entity->getEntityTypeId(), $entity->bundle());
  }

}
